<?php

require_once 'classes.php';
require_once 'DataBase.php';

$db = new DataBase();
$result = $db->query("SELECT * FROM product ORDER BY sku");
$db->disconnection();
foreach ($result as $row) {
    $type = $row['type'];
    $obj = new $type();
    $check = $obj->Setter($row, 'read');
    if ($check) $obj->PrintProduct();
}
