function changewindow(win)
{
	var www = document.getElementById("win");
	www.remove();

	switch(win.value) {
		case "dvd": dvd();break;
		case "book": book();break;
		case "furniture": furniture();break;
		default: empty();
	}
}
	
function empty() 
{
	var d1 = document.createElement("div");
	d1.id = "win";
	d1.innerHTML = "Please, provide the type of product";
	document.getElementById("par").insertBefore(d1, document.getElementById("jakor"));
}

function dvd() 
{
	var d1 = document.createElement("div");
	d1.id = "win";
	d1.innerHTML = "<label for='size'>Size(MB) </label><input name='size' class='inp' type='text' placeholder='#size' id='size'><br/><br/>";
	document.getElementById("par").insertBefore(d1, document.getElementById("jakor"));
}

function book() 
{
	var d1 = document.createElement("div");
	d1.id = "win";
	d1.innerHTML = "<label for='weight'>Weight(KG) </label><input name='weight' class='inp' type='text' placeholder='#weight' id='weight'><br/><br/>";
	document.getElementById("par").insertBefore(d1, document.getElementById("jakor"));
}

function furniture() 
{
	var d1 = document.createElement("div");
	d1.id = "win";
	d1.innerHTML = "<label for='height'>Height(CM) </label><input name='height' class='inp' type='text' placeholder='#height' id='height'><br/><br/><label for='width'>Width(CM) </label><input name='width' class='inp' type='text' placeholder='#width' id='width'><br/><br/><label for='length'>Length(CM) </label><input name='length' class='inp' type='text' placeholder='#length' id='length'><br/><br/>";
	document.getElementById("par").insertBefore(d1, document.getElementById("jakor"));
}
