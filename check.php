<?php

require_once 'classes.php';

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $type = $_POST['type'];
    if ($type != 'type') {
        $obj = new $type();
        $check = $obj->Setter($_POST, 'write');
        if ($check) $obj->SqlWrite();
    }
}
