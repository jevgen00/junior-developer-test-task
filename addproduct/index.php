<?php
require_once "../check.php";
?>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Product List</title>
<link rel="stylesheet" type="text/css" href="../style.css" />
<script src="../check.js"></script>
</head>
<body>

<button onclick="document.location='http://jevgeni.tk/'" id="" class="button" value="cancel">Cancel</button>
<input type="submit" form="product_form" class="button" style="margin-right:15px;" onclick="ter()" value="Save" />

<h1>Product Add</h1>
<hr size="2" color="black">

<form name="form" id="product_form" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>" method="post">
    <div>
        <div class="main" id="par">
            <div class="field">
                <label for="sku">SKU </label><input name="sku" class="inp" type="text" placeholder="#sku" value="<?php echo (isset($_POST['sku']) ? $_POST['sku'] : ''); ?>" id="sku"><br/><br/>
            </div>
            <div class="field">
                <label for="name">Name </label><input name="name" class="inp" type="text" placeholder="#name" value="<?php echo (isset($_POST['name']) ? $_POST['name'] : ''); ?>" id="name"><br/><br/>
            </div>
            <div class="field">
                <label for="price">Price($) </label><input name="price" class="inp" type="text" placeholder="#price" value="<?php echo (isset($_POST['price']) ? $_POST['price'] : ''); ?>" id="price"><br/><br/>
            </div>
            <div class="field">
                <label for="type">Type Switcher </label>
                <select name="type" class="inp" id="productType" onchange="changewindow(this)">
                    <option <?php if(isset($_POST['type']) && $_POST['type'] == 'type') echo 'selected'; ?> id="#type" value="type">Type Switcher</option>
                    <option <?php if(isset($_POST['type']) && $_POST['type'] == 'dvd') echo 'selected'; ?> id="#DVD" value="dvd">DVD</option>
                    <option <?php if(isset($_POST['type']) && $_POST['type'] == 'book') echo 'selected'; ?> id="#Book" value="book">Book</option>
                    <option <?php if(isset($_POST['type']) && $_POST['type'] == 'furniture') echo 'selected'; ?> id="#Furniture" value="furniture">Furniture</option>
                </select>
            </div>
            <br/>
            <div id="win">

                <?php

                    if(isset($_POST['type'])) {
                        switch ($_POST['type']) {
                            case 'type':
                                ?>
                                Please, provide the type of product<br/>
                                <?php
                                break;
                            case 'dvd':
                                ?>
                                    <label for="size">Size(MB) </label>
                                    <input name="size" class="inp" type="text" placeholder="#size" value="<?php echo (isset($_POST['size']) ? $_POST['size'] : ''); ?>" id="size">
                                    <br><br>
                                <?php
                                break;
                            case 'book':
                                ?>
                                    <label for="weight">Weight(KG) </label>
                                    <input name="weight" class="inp" type="text" placeholder="#weight" value="<?php echo (isset($_POST['weight']) ? $_POST['weight'] : ''); ?>" id="weight">
                                    <br><br>
                                <?php
                                break;
                            case 'furniture':
                                ?>
                                    <label for="height">Height(CM) </label>
                                    <input name="height" class="inp" type="text" placeholder="#height" value="<?php echo (isset($_POST['height']) ? $_POST['height'] : ''); ?>" id="height">
                                    <br><br>
                                    <label for="width">Width(CM) </label>
                                    <input name="width" class="inp" type="text" placeholder="#width" value="<?php echo (isset($_POST['width']) ? $_POST['width'] : ''); ?>" id="width">
                                    <br><br>
                                    <label for="length">Length(CM) </label>
                                    <input name="length" class="inp" type="text" placeholder="#length" value="<?php echo (isset($_POST['length']) ? $_POST['length'] : ''); ?>" id="length">
                                    <br><br>
                                <?php
                                break;
                        }
                    } else {
                        ?>
                        Please, provide the type of product<br/>
                        <?php
                    }

                ?>

			</div>
			<div id="jakor"></div><br /><br />
		</div>	
		<hr size="2" color="black" style="width:100%;">
		<p style="text-align: center;">Scandiweb Test assignment</p>
	</div>
</form>

<script src="../window.js"></script>
</body>
</html>
