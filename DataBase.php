<?php

class DataBase
{
    private $link;

    public function __construct()
    {
        $this->connection();
    }

    public function connection()
    {
        $config = include 'config.php';
        $dsn = 'mysql:host='.$config['host'].';dbname='.$config['db_name'].';charset='.$config['charset'];
        $this->link = new PDO($dsn, $config['username'], $config['password']);
        return $this;
    }

    public function query($sql)
    {
        $sth = $this->link->prepare($sql);
        $sth->execute();
        $result = $sth->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }

    public function disconnection()
    {
        $this->link = null;
    }
}
