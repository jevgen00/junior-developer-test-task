<?php
require_once 'DataBase.php';

abstract class Product
{
    private $type;
    private $sku;
    private $name;
    private $price;

    function PrintProduct()
    {
        ?>
            <div class='light'>
                <input type='checkbox' class='delete-checkbox' value="<?php echo $this->sku; ?>" name="<?php echo $this->sku; ?>" >
                <div class='info'>
                    <p><?php echo $this->sku; ?></p>
                    <p><?php echo $this->name; ?></p>
                    <p><?php echo $this->price; ?></p>
        <?php
    }

    function SetParentInfo($info, $purpose)
    {
        $skuCheck = $this->setSku($info['sku'], $purpose);
        $nameCheck = $this->setName($info['name']);
        $priceCheck = $this->setPrice($info['price']);
        $typeCheck = $this->setType($info['type']);
        return ($skuCheck && $nameCheck && $priceCheck && $typeCheck) ? true : false;
    }

    function setSku($sku, $purpose)
    {
        switch ($purpose){
            case 'write':
                if ($sku != '' && strlen($sku)<=20) {
                    $db = new DataBase();
                    $result = $db->query("SELECT sku FROM product");
                    $db->disconnection();
                    foreach ($result as $row) {
                        if ($sku == $row['sku']) {
                            ?> <script type="text/javascript"> alert("This SKU code is already used"); </script> <?php
                            return false;
                        }
                    }
                    $this->sku = $sku;
                    return true;
                } else {
                    return false;
                }
                break;
            case 'read':
                if ($sku != '' && strlen($sku)<=20) {
                    $this->sku = $sku;
                    return true;
                } else {
                    return false;
                }
                break;
        }
        
    }
    function setName($name)
    {
        if ($name != '' && strlen($name)<=20) {
            $this->name = $name;
            return true;
        } else {
            return false;
        }
    }
    function setPrice($price)
    {
        if ($price != '' && is_numeric($price)) {
            $this->price = $price;
            return true;
        } else {
            return false;
        }
    }
    function setType($type)
    {
        if ($type == 'dvd' || $type == 'book' || $type == 'furniture') {
            $this->type = $type;
            return true;
        } else {
            return false;
        }
    }

    function __get($property)
    {
        return $this->$property;
    }
}

class dvd extends Product
{
    private $size;

    function SqlWrite()
    {
        $type = parent::__get('type');
        $sku = parent::__get('sku');
        $name = parent::__get('name');
        $price = parent::__get('price');
        $db = new DataBase();
        $success = $db->query("INSERT INTO product (Type, SKU, Name, Price, Size) VALUES ('$type', '$sku', '$name', '$price', '$this->size')");
        $db->disconnection();
        if ($success === false) {
            echo('Error writing to database');
		} else {
		    header("Location: http://jevgeni.tk/");
		}
    }
	
    function PrintProduct()
    {
        parent::PrintProduct();
        ?>
                    <p>Size: <?php echo $this->size; ?> MB</p>
                </div>
            </div>
            <?php
    }

    function Setter($info, $purpose)
    {
        $parentCheck = parent::setParentInfo($info, $purpose);
        $sizeCheck = $this->setSize($info['size']);
        return ($parentCheck && $sizeCheck) ? true : false;
    }

    function setSize($size)
    {
        if ($size != '' && is_numeric($size) && $size > 0) {
            $this->size = $size;
            return true;
        } else {
            return false;
        }
    }
}

class book extends Product
{
    private $weight;

    function SqlWrite()
    {
        $type = parent::__get('type');
        $sku = parent::__get('sku');
        $name = parent::__get('name');
        $price = parent::__get('price');
        $db = new DataBase();
        $success = $db->query("INSERT INTO product (Type, SKU, Name, Price, Weight) VALUES ('$type', '$sku', '$name', '$price', '$this->weight')");
        $db->disconnection();
        if ($success === false) {
            echo('Error writing to database');
		} else {
            header('Location: http://jevgeni.tk/');
        }
    }

    function PrintProduct()
    {
        parent::PrintProduct();
        ?>
                    <p>Weight: <?php echo $this->weight; ?> KG</p>
                </div>
            </div>
        <?php
    }

    function Setter($info, $purpose)
    {
        $parentCheck = parent::setParentInfo($info, $purpose);
        $weightCheck = $this->setWeight($info['weight']);
        return ($parentCheck && $weightCheck) ? true : false;
    }

    function setWeight($weight)
    {
        if ($weight != '' && is_numeric($weight) && $weight > 0) {
            $this->weight = $weight;
            return true;
        } else {
            return false;
        }
    }
}

class furniture extends Product
{
    private $height;
    private $width;
    private $length;

    function SqlWrite()
    {
        $type = parent::__get('type');
        $sku = parent::__get('sku');
        $name = parent::__get('name');
        $price = parent::__get('price');
        $db = new DataBase();
        $success = $db->query("INSERT INTO product (Type, SKU, Name, Price, Height, Width, Length) VALUES ('$type', '$sku', '$name', '$price', '$this->height', '$this->width', '$this->length')");
        $db->disconnection();
        if ($success === false) {
            echo('Error writing to database');
		} else {
            header('Location: http://jevgeni.tk/');
        }
    }

    function PrintProduct()
    {
        parent::PrintProduct();
        ?>
                    <p>Dimension: <?php echo $this->height; ?>x<?php echo $this->width; ?>x<?php echo $this->length; ?></p>
                </div>
            </div>
        <?php
    }

    function Setter($info, $purpose)
    {
        $parentCheck = parent::setParentInfo($info, $purpose);
        $heightCheck = $this->setHeight($info['height']);
        $widthCheck = $this->setWidth($info['width']);
        $lengthCheck = $this->setLength($info['length']);
        return ($parentCheck && $heightCheck && $widthCheck && $lengthCheck) ? true : false;
    }

    function setHeight($height)
    {
        if ($height != '' && is_numeric($height) && $height > 0) {
            $this->height = $height;
            return true;
        } else {
            return false;
        }
    }
    function setWidth($width)
    {
        if ($width != '' && is_numeric($width) && $width > 0) {
            $this->width = $width;
            return true;
        } else {
            return false;
        }
    }
    function setLength($length)
    {
        if ($length != '' && is_numeric($length) && $length > 0) {
            $this->length = $length;
            return true;
        } else {
            return false;
        }
    }
}
