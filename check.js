function ter()
{
	var type;
	var text1;

	text1 = document.form.sku.value;
	if (!text1) {alert("Please, provide the SKU code"); return;}
	else if (text1.length > 20) {alert("Please, check the SKU code"); return;}

	text1 = document.form.name.value;
	if (!text1) {alert("Please, provide the Name"); return;}
	else if (text1.length > 20) {alert("Please, check the Name"); return;}

	text1 = document.form.price.value;
	if (!text1) {alert("Please, provide the Price"); return;}
	else if (isNaN(text1)) {alert("Please, check the Price"); return;}
	else if (text1 < 0) {alert("Please, check the Price"); return;}
	
	type = document.form.type.value;
	switch (type)
	{
		case "type": alert ("Please, provide the type of product");
		    break; 
			
		case "dvd":
			text1 = parseInt(document.form.size.value);
			if (!text1 || !Number.isInteger(text1) || text1 < 0) {alert("Please, check the Size");}
			break;

		case "book":
			text1 = parseInt(document.form.weight.value);
			if (!text1 || !Number.isInteger(text1) || text1 < 0) {alert("Please, check the Weight");}
			break;

		case "furniture":
			text1 = parseInt(document.form.height.value);
			text2 = parseInt(document.form.width.value);
			text3 = parseInt(document.form.length.value);
			if (!text1 || !text2 || !text3) {alert("please, provide all product dimensions");}
			else if ((!Number.isInteger(text1) || text1 < 0) || (!Number.isInteger(text2) || text2 < 0) || (!Number.isInteger(text3) || text3 < 0)) {alert("Please, check the Sizes");}
			break;
	}
}
